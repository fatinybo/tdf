+++
title = "C'est bientôt le départ"
date = "2017-09-02"
tags = ["intro"]
categories = ["Avant le départ"]
banner = "img/banners/kraft.jpg"
+++

## Deux mois de ballades
Normalement, si tout se passe bien au niveau de l'immigration, notre départ pour Montréal devrait avoir lieu durant la première quinzaine de décembre. Cela nous laisse donc quelques semaines pour parcourir la France, en long en large et en travers ! Nous sommes momentanément un couple de chômeurs-SDF-heureux, alors nous profitons de ces quelques mois de latence pour venir à votre rencontre ! Visiter notre beau pays, faire des bisous à nos familles et amis, en voici en chouette programme !


## L'agenda

[Il est là !](https://framagenda.org/index.php/apps/calendar/p/6PT4EPJ5N8MEGH8E)

Nous n'avons pas souhaité nous implanter de puce GPS sous la peau, alors si vous souhaitez nous suivre un peu, l'agenda est d'une grande aide !


## Le sens du tour
Grosso modo, nous allons faire le tour dans le sens trigonométrique (c'est à dire, sens inverse des aiguilles d'une montre pour ceux qui auraient quelques difficultés en mathématiques :p ).


## Le sens du blog
N'étant pas très attachés à nos téléphones portables, préférant profiter de l'instant présent, nous ne pouvons pas donner de nouvelles à tous, tout le temps ! Alors ce blog pourra vous permettre de suivre notre périple, si le cœur vous en dit !
Aussi, c'est un entraînement pour notre blog canadien !


Des bises à tous !

Fatinybo.


{{< youtube 2YXDd_NKhac >}}