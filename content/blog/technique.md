+++
title = "Un peu de technique sur ce blog"
date = "2017-09-07"
tags = ["technique"]
categories = ["Avant le départ"]
banner = ""
draft = true
+++

## Hugo : un moteur de blog statique
Il y a quelques années, la mode était aux Wordpress. Il permet de déployer facilement un blog disposant d'une interface d'administration en ligne. De ce fait, la publication est aisée. Cependant, c'est une belle usine à gaz. D'autant plus que la majorité des blogs n'ont pas besoin d'être dynamiques (c'est-à-dire ne nécessite pas d'avoir un contenu alloué en fonction de base de donnée ou ne nécessite pas de login).

Alors autant passer sur un site statique ! Les avantages sont multiples : hébergement plus aisé, moins de risques d'attques (on sort de ce PHP pourave, pour n'avoir que du simple HTML) et pas de PHP. Quelques moteurs de blog statique ont alors fait leur apparition : Jekyll, Hexo et Hugo pour ne citer qu'eux. 

Pour ce site, j'ai choisi Hugo. Il est écrit en Go, semble très rapide et sympathique à utiliser. Pour l'installation


## Le Markdown pour rédiger des posts


