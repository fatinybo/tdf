+++
title = "Balade autour du lac du Salagou"
date = "2017-09-15"
tags = []
categories = ["Randonnées"]
banner = "img/blog/salagou/IMG_3153_web.jpg"
+++

Vendredi, bien décidés à continuer de découvrir cette belle région, nous prévoyons une randonnée au bord du lac du Salagou, au nord de Montpellier. Super chouette, aujourd'hui Noémie s'auto-dispense de cours pour se joindre à nous ! 

Après avoir englouti les fameux pancakes végans de Nono (un délice), le brainstorming pour choisir quel chemin nous suivrons a lieu. Le parcours retenu fait 14 km, sur la partie nord du lac, aux alentours du village des Vailhés.  
Chaussures aux pieds, Nono plus motivée que jamais à aller fouler la terre rouge du lac, nous voilà en route tous les 3. Après s'être un peu perdus en route (encore!), nous avons trouvé le lac. Wahou !

<a href="/tdf/img/blog/salagou/lac_web.jpg"> <img src="/tdf/img/blog/salagou/lac_web.jpg" class="img-thumbnail center-block" alt="lac" width="60%"></a>

Le las du Salagou a plusieurs particularités : la première, son nom est très mignon, n'est-ce pas ?! La deuxième, c'est un lac artificiel qui est apparu suite à la construction d'un barrage dans les années 1950. Ce barrage avait plusieurs intérêts. Tout d'abord, il a permis de diversifier les cultures en développant la production fruitière, la viticulture devenant surproductive. Aussi, le barrage permet de réguler les crues de l'Hérault. Enfin, le lac du Salagou sert aux canadairs pour se ravitailler en eau (Oh feu les pompiers dans cette région très sèche!). 

C'est parti pour la randonnée ! Nous longeons le lac pendant la première moitié de la randonnée. Le paysage est très surprenant, lunaire ! Le sol est rouge grâce à la ruffe : une roche rouge formée par la combinaison de sédiments argileux et d'oxyde de fer. Nous ne savons pas si nous avons atterris sur la lune ou à Rolland Garros ! L'ensemble est très vallonné, nous montons, descendons, remontons, redescendons ...etc ! Les nuages sont au rendez-vous, nous n'avons pas la chance d'observer le bleu du ciel se refléter dans le lac, mais nous en prenons quand même plein les yeux !
Passage autour de la chapelle Notre Dame des Clans, bâtie au XIIe siècle, elle est le sujet de légendes mélangeant une châtelaine, une mule, un muletier et un orage ! Si vous êtes tenus en haleine ... [cliquez ici !](http://www.celles-salagou.org/municipale/index.php?pages/La-chapelle-N.D.-de-Clans)

<a href="/tdf/img/blog/salagou/eglise_web.jpg"> <img src="/tdf/img/blog/salagou/eglise_web.jpg" class="img-thumbnail center-block" alt="eglise" width="60%"></a>

Les sols ferrigineux qui bordent le lac ne sont pas dépourvus de végétation. Nous reconnaissons notamment les figuiers de barbarie remplis de fruits : des figues de Barbarie ! Nous croisons de nombreuses crottes d'animaux pleines de graines de ces fruits...! Nono et moi avons décortiqué un fruit,           /!\ attention aux minis épines (appelées glochides) /!\ et l'avons goutté ! MIAM, ils ont raison ces animaux.  /!\ Deuxième mise en garde, la chair peut provoquer des occlusions intestinales car les graines sont très épaisses ! Gare à vous, vous êtes avertis ! Dernière information, l'huile de pépins de figue de barbarie est l'huile la plus concentrée en vitamine E, ce qui en fait un puissant anti-oxydant, jeunesse éternelle !! 

<a href="/tdf/img/blog/salagou/figues_web.jpg"> <img src="/tdf/img/blog/salagou/figues_web.jpg" class="img-thumbnail center-block" alt="figues" width="60%"></a>

Trêve de plaisanterie, nous avons une promenade à boucler ! 
Nous quittons les bords du lac, nous prenons de la hauteur, c'est beau n'est-ce pas ?!

<a href="/tdf/img/blog/salagou/tetes_web.jpg"> <img src="/tdf/img/blog/salagou/tetes_web.jpg" class="img-thumbnail center-block" alt="tetes" width="60%"></a>

Nous continuons le chemin, entourés de vignes, nous sommes sur un plateau. Nous passons au dessus du barrage après avoir déniché un spot "P.D.V." (=Point De Vue !) pour casser la croûte en admirant le lac avec de la hauteur. Là, nous sommes un peu perdus ! Nous continuons sur ce chemin de hallage en espérant trouver la capitelle ! Le périple se poursuit avec une super rencontre ! Nous sommes arrivés à côté d'un près où nos amis les ânes prennent du bon temps ! L'un d'entre eux brait en nous voyant nous approcher et toute la compagnie s'est précipitée vers nous ! Câlins, grattouilles, quelques grandes discussions et de superbes photos au rendez-vous !

<a href="/tdf/img/blog/salagou/ane_web.jpg"> <img src="/tdf/img/blog/salagou/ane_web.jpg" class="img-thumbnail center-block" alt="ane" width="60%"></a>

<a href="/tdf/img/blog/salagou/ANE_web.jpg"> <img src="/tdf/img/blog/salagou/ANE_web.jpg" class="img-thumbnail center-block" alt="ANE" width="60%"></a>

Après cet instant hors du temps avec les ânes, nous finissons par trouver la fameuse capitelle  ! Ces cabanes en pierre sèche servaient autrefois à abriter temporairement les petits propriétaires, leurs outils et leurs récoltes.

Nous marchons toujours sur le plateau, cela fait bien longtemps que nous n'avons pas vu le lac du Salagou, il commence à nous manquer ! Nous contournons le "champ carré", suivons les lignes électriques, faisons confiance au chemin ...etc ... (= les indications du guide!), pour enfin redescendre et apercevoir à nouveau le lac. La fin de la randonnée nous fait passer par un petit hameau qui est à un tiers rénové, un tiers en ruine et un tiers en rénovation. En tout cas tout ce joyeux mélange est bien mignon.

<a href="/tdf/img/blog/salagou/hameau_web.jpg"> <img src="/tdf/img/blog/salagou/hameau_web.jpg" class="img-thumbnail center-block" alt="hameau" width="60%"></a>

La randonnée se termine en longeant le lac, jusqu'à la clio !

Ce fut encore une bien belle journée, en compagnie de la super Nono.

À retenir : des scènes du film "Rrrrrr" de Alain Chabat ont été tournées sur les bords du lac du Salagou !


<a href="/tdf/img/blog/salagou/tortue_web.jpg"> <img src="/tdf/img/blog/salagou/tortue_web.jpg" class="img-thumbnail center-block" alt="tortue" width="60%"></a>

<a href="/tdf/img/blog/salagou/vue_1_web.jpg"> <img src="/tdf/img/blog/salagou/vue_1_web.jpg" class="img-thumbnail center-block" alt="vue_1" width="60%"></a>


<a href="/tdf/img/blog/salagou/vue_2_web.jpg"> <img src="/tdf/img/blog/salagou/vue_2_web.jpg" class="img-thumbnail center-block" alt="vue_2" width="60%"></a>

<a href="/tdf/img/blog/salagou/fleur_web.jpg"> <img src="/tdf/img/blog/salagou/fleur_web.jpg" class="img-thumbnail center-block" alt="fleur" width="60%"></a>

<a href="/tdf/img/blog/salagou/vue_3_web.jpg"> <img src="/tdf/img/blog/salagou/vue_3_web.jpg" class="img-thumbnail center-block" alt="vue_3" width="60%"></a>
