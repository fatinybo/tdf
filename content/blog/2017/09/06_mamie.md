+++
title = "Première étape : Hiis, chez la mamie du Titi"
date = "2017-09-06"
tags = []
categories = ["Famille"]
banner = "img/blog/mamie_head_web.jpg"
+++

Nous voilà enfin réellement partis pour notre fameux tour de France, qui fait déjà couler beaucoup d'encre !  
La voiture est prête : pression des pneus, ampoules vérifiées et changées (merci Gégé) et aspi passé ! La voilà pleine à craquer, impatiente d'avaler les kilomètres !  
L'objectif de la fin de cette dernière semaine : la ville rose, Toulouse (enfin, ses environs).

Mercredi matin, avec 1h de retard sur l'heure annoncée du départ, let's go baby !

Mamie nous attend avec un poulet rôti, des haricots verts et des tomates, tout ça de sa production personnelle ! Un régal !  
Nous avons passé un très chouette moment à ses côtés. Elle nous a fait essayer les paires de chaussettes en laine qu'elle nous tricote gentiment en prévision de notre départ pour le grand froid !  
Elle s'inquiète un peu pour nous : la voiture, les dodos en tente, mais surtout, elle se demande si nous n'allons pas devenir "des hippies" !

Chaussettes de laine aux pieds (avec nos shorts respectifs, les grands look !), mamie rassurée, ventre bien rempli : nous nous remettons en route pour Pamiers.