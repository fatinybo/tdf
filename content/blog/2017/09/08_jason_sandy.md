+++
title = "Jason & Sandy"
date = "2017-09-08"
tags = []
categories = ["Amis"]
banner = "img/blog/jason_head_web.jpg"
+++

Entre Pamiers et Drémil-Lafage nous avons, encore une fois, été gâtés par les paysages. Les routes sinueuses entourées de moultes platanes n'ont pas été idéales pour m'aider à rédiger la conclusion de mon mémoire, mais Titi s'est régalé, en mode "pilote de rallye"! Les briques roses et les clochers occitans sont au rendez-vous, avec un petit coup de coeur pour le village de Calmont.

Nous voilà arrivés à bon port, accueillis par Maïko, le chien de la famille !  Cela faisait 4 ans que nous ne nous étions pas vus. Les retrouvailles, dans leur maison toute mignonne, furent un vrai régal ! Les hommes aux fourneaux et les femmes sagement en train de discuter : le bonheur !! 

Malgré une soirée (un peu) arrosée, une heure tardive de couché et le mauvais temps, nous étions dans nos baskets pour un footing décrassage de 8 km samedi matin ! C'est assez agréable de découvrir la campagne française en courant ! 

Nous voulions aller visiter la cité de l'espace samedi après-midi, mais la météo n'a pas du tout été clémente. C'était un temps à jeux de société : du coup, j'ai ridiculisé les gars au [Gabu](https://fr.wikipedia.org/wiki/Tamalou) pendant que Sandy faisait la sieste ! La partie a été tellement prenante que nous sommes partis en retard pour aller cueillir Noémie et Pierre de l'autre côté de Toulouse ! Oups !   
Les jeunes embarqués, la prochaine étape du périple est Léguevin, banlieu ouest de Toulouse, pour un week-end en famille.

