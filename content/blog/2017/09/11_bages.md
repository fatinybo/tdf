+++
title = "La première nuit en camping sauvage !"
date = "2017-09-11"
tags = []
categories = ["Aventures"]
banner = "img/blog/camping_web.jpg"
+++

Cette journée est sous le signe des CHÂTEAUX CATHARES ! Nous traversons successivement les départements des Hautes-Pyrénées, une toute petite section du sud-ouest du Tarn et l'Aude.  
La route est superbe, nous sommes au milieu de la forêt, la végétation est riche, chênes et châtaigniers arborent la route en lacets. 

Première pause à Saissac, petite commune de l'Aude en région Occitanie. Ce petit village pittoresque de la Montagne Noire a conservé des fortifications féodales et des ruines d'un château fort : nous voulions voir ça ! Le château de Saissac est l'un des plus vastes du Languedoc, il est a flanc de montagne, domine la vallée, c'est assez impressionnant. Nous avons fait un petit tour dans ce village qui permet d'entretenir les fessiers tellement il est pentu ! C'est mignon, mais nous nous y sentons assez oppressés, il y a une super vue mais dans les ruelles encastrées nous ne pouvons pas en profiter. Café, pipi et c'est reparti !   

Nous continuons sur les routes départementales de l'Aude, les paysages sont riches de diversités. Les vignes (AOC Cabardès), les forêts, puis des paysages plus arides, avec une végétation basse et plein de cailloux ! Nous arrivons à Lastours, en quête de châteaux cathares ! Ici nous ne pouvons pas être déçus, la ville en compte 4 ! Ce petit village est chargé d'histoire, les premiers vestiges connus remontent à l'Âge de Bronze (vers 1500 avant JC). Trois des quatre châteaux sont bâtis au XIIe siècle au régime de la co-seigneurie. Ces trois châteaux connaîtront les assauts des croisés, résistant dans un premier temps puis capitulant ensuite. À la fin des Croisades, en représailles, les troupes royales détruisent le village et les châteaux, qui sont ensuite reconstruits sur la crête. La suprématie du roi est affirmée par la construction d'un 4e château !  
En 1905, les quatre châteaux sont classés Monuments Historiques.
Si certains d'entre vous trouvent cela passionnant : [pour continuer la visite](http://www.chateauxdelastours.fr/topic/index.html)

PHOTO CHATEAU

Après cet instant culturel, les cheveux décoiffés par le vent qui ne souffle pas en flemme ici, nous retrouvons notre fidèle Clio et nous partons en direction du Somail. Pour rejoindre ce hameau de l'Aude situé sur le canal du Midi, nous quittons le Carcassonnais pour gagner les plaines du Minervois, pays de vin ! La fin d'après-midi approche, nous remplissons la glacière en prévision du casse-croûte de ce soir, et nous espérons que le Somail nous offrira un petit havre de paix pour planter la tente ! Bon, ce petit hameau est certes très mignon, il n'est pas inscrit aux Monuments Historiques pour rien... mais c'est un endroit de passage où de nombreux retraités en camping-car ont aussi décidé de se poser ! Échec de la nuit au bord du canal du Midi ! Une pause en terrasse s'impose, rassemblons nos idées pour trouver un plan B !<br/>
Après un moment d'errance dans le vignoble aux alentours du Somail, nous décidons de nous diriger vers Bages, au sud de Narbonne : mon cousin Yann nous avait conseillé ce coin pour camper si besoin.  
Nous voilà arrivés à Bages, ce petit village à 10km au sud de Narbonne seulement est d'un calme et d'une beauté auxquels nous ne nous attendions pas ! Perché, juste au bord du lac de Bages-Sigean, bordé par les vignes, ce coin nous inspire confiance ! Surtout que le soleil ne va plus trop tarder à se coucher, il devient urgent pour nous de trouver notre spot ! Nous traversons le village et allons nous perdre sur le hauteurs alentours. Le premier test de "plantage de tente" dans un petit coin avec une vue dominant le lac et le village fut un échec : impossible de planter une seule sardine dans ce sol rocailleux et le vent souffle beaucoup trop fort ! Tant pis pour la vue au réveil demain matin, on redescend un peu ! Finalement nos installons notre campement dans un petit champ, au milieu des parcelles de vignes. Le vent souffle toujours, et nous plantons les sardines tant bien que mal ! Nous avons des progrès à faire en camping sauvage ! Petit casse-croûte, on observe un peu les étoiles et la voie lactée : le ciel est bien dégagé et nous ne sommes pas victimes de la pollution lumineuse. 22h : au lit. La nuit n'a pas été reposante du tout ! Le vent a soufflé beaucoup trop fort, imposant un bruit infernal dans la tente !!! Nous sommes réveillés par les bruits de tracteurs et les travailleurs qui viennent vendanger. L'un d'entre eux cri même "DEBOUUUUT, DEBOUUUUUT !!" avec malice !  
Café, on remballe le camp et nous retournons au premier spot, tout en haut, pour admirer la vue ! Nous avons les yeux fatigués, mais nous devons reprendre la route pour arriver à Montpellier à 14h pour le rdv dentiste de Titi !  
[Bages](https://fr.wikipedia.org/wiki/Bages_(Aude))

PHOTOS

Let's go : Narbonne, Bézier, Agde (petit arrêt pour voir : pas jojo d'après nous!), Sète, Montpellier.
