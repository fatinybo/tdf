+++
title = "Le cirque de Navacelle"
date = "2017-09-13"
tags = []
categories = ["Randonnées"]
banner = "img/blog/nav_fanny_marche_web.jpg"

+++

Nous avons retrouvé nos deux petits : Noémie et Pierre, qui nous hébergent (à titre gratuit!) gentiment du mardi 12 au lundi 18 septembre. C'est parti pour une semaine montpellièraine au côté de ma moyenne petite sœur adorée préférée :)  
Mardi après-midi nous nous sommes un peu promenés dans la ville, sous un beau et chaud soleil. Nous en avons profité pour quérir des guides de randos des alentours. Bien décidés à transpirer : nous programmons une randonnée le mercredi, autour du cirque de Nacavelle, dans la partie méridionale des Grandes Causses, au sud du Massif Central.    
Le cirque de Navacelle, classé au patrimoine mondial de l'Unesco est une merveille géologique considérée comme l'un des plus grands canyons d'Europe ! Il a été creusé dans le calcaire par la rivière la Vis durant 2 à 3 millions d'années !  
Nous nous lançons dans une randonnée de 27km, même pas peur ! Voiture garée à Saint-Maurice-de-Navacelle, c'est parti pour la promenade la plus longue de notre vie !!   
Le début se passe sans embûches, le sentier est balisé ! Nous marchons sur un sentier calcaire, entouré de nombreuses ronces pleines de mûres (miam!). La végétation est assez basse et sèche. Les couleurs sont superbes : automnales, nuances de verts, de marrons et de bleu du ciel.   
Après 2h de marche nous atteignons le hameau du Coulet : perdu au milieu de la campagne, il a gardé toute son authenticité. Nous entendons les biquettes mais nous ne les voyons pas, snif ! Après 1,5km de marche, accompagnés par de nombreux papillons et autres sauterelles multicolores, nous nous apercevons que nous avons fait mauvaise route : demi-tour ! Petite pause casse-croûte avant de repartir, sur le bon chemin !! 

<a href="/tdf/img/blog/nav_village_le_coulet_web.jpg"> <img src="/tdf/img/blog/nav_village_le_coulet_web.jpg" class="img-thumbnail center-block" alt="le coulet" width="60%"></a>

Nous prenons de la hauteur et cela nous offre de beaux points de vue sur les massifs. Le sentier n'est plus balisé, nous avons encore une fois fait une erreur de trajectoire, mais cette fois-ci nous sommes retombés sur nos pattes sans même nous en rendre compte !

<a href="/tdf/img/blog/nav_fanny_vue_web.jpg"> <img src="/tdf/img/blog/nav_fanny_vue_web.jpg" class="img-thumbnail center-block" alt="st_loup_hortus" width="60%"></a>

Arrivés sur les ruines d'un village, qui a priori a abrité des résistants, nous avons une belle vue sur le Pic-Saint-Loup et l'Hortus comme vous pouvez le constater sur la photo !

<a href="/tdf/img/blog/nav_st_loup_hortus_web.jpg"> <img src="/tdf/img/blog/nav_st_loup_hortus_web.jpg" class="img-thumbnail center-block" alt="st_loup_hortus" width="60%"></a>

Nous continuons à prendre de la hauteur, les points de vue sur la région sont superbes, surtout que le soleil est généreux ! Nous n'oublions pas l'objectif de cette randonnée : observer le cirque de Navacelle ! Nous commençons activement notre recherche mais malgré les kilomètres qui s'engrangent au compteur, toujours pas de cirque en vue ! A priori d'après la carte du (maudit!) guide de rando, nous marchons sur les hauteurs d'un cirque, que nous pensions être celui de Navacelle ! Le sol que nous foulons intrigue beaucoup Titi : d'énormes blocs de caillasse troués de part et d'autre. Je suis navrée, malgré les nombreuses heures de géologie que j'ai subi en prépa, je ne suis pas capable de vous en dire plus sur la nature géologique de ces cailloux !
Nous sommes entourés de touffes de thym et de lavande : huuuum ça sent bon !

<a href="/tdf/img/blog/nav_hameau_web.jpg"> <img src="/tdf/img/blog/nav_hameau_web.jpg" class="img-thumbnail center-block" alt="st_loup_hortus" width="60%"></a>

Un gros rocher surplombé par une croix nous inspire et nous appelle à faire une pause pour admirer la vue. Nous sommes assis au bord d'un cirque, à l'abri du vent, la vue est magnifique.

<a href="/tdf/img/blog/nav_fanny_vue_web.jpg"> <img src="/tdf/img/blog/nav_vue_web.jpg" class="img-thumbnail center-block" alt="st_loup_hortus" width="60%"></a>

Après ce petit instant de repos, bien restaurés, nous repartons tête baissée, plein d'énergie !... Trop d'énergie et de détermination peut être ?! Après 1h de grimpette relativement difficile, oh, surprise : sur notre gauche, au loin, LE CIRQUE DE NAVACELLE !!! Là, nous n'avons plus de doutes, c'est bien lui ! Par contre, la direction que nous avons pris nous a visiblement bien éloignés du but ! Photo ci-dessous :


<a href="/tdf/img/blog/nav_cirque_web.jpg"> <img src="/tdf/img/blog/nav_cirque_web.jpg" class="img-thumbnail center-block" alt="st_loup_hortus" width="60%"></a>

<a href="/tdf/img/blog/nav_bestiole_rouge_web.jpg"> <img src="/tdf/img/blog/nav_bestiole_rouge_web.jpg" class="img-thumbnail center-block" alt="st_loup_hortus" width="60%"></a>

Photo bestiole : un compagnon pendant notre pique-nique, tout joli !

Nous avons bien pris de la hauteur, enfin trouvé le cirque de Navacelle, maintenant il ne nous reste plus qu'à trouver le chemin... Donc, demi-tour, remplis de doutes ! Finalement, nous avions juste ratés la marque après avoir mangé !  
Nous voilà à nouveau sur la bonne route, nous descendons en traversant des près mais nous n'avons croisé aucune bébête ! À la fin de ce petit chemin cabossé nous atteignons une plateau plein de jolies vaches qui paîtrent ici tranquillement ! Nous ne sommes jamais allés au Texas ou en Arizona, mais nous avons l'impression d'y être !!  
La route continue, nos pieds commencent à nous en vouloir ! Nous remontons sur un petit sentier forestier, maintenant ce sont nos fessiers qui commencent sérieusement à nous en vouloir !!  
Sur les 5 derniers kilomètres nous croisons un gîte bien caché, des moutons très timides et surtout, nous sommes beaucoup plus proches du cirque ! Le soleil l'éclaire à merveille, il est somptueux. Nous le longeons sur les derniers kilomètres, Titi n'en peut plus !!  

Après 28 km dans les pattes, avec 1500 m de dénivelé positif, la boucle est bouclée ! Nous n'avons plus qu'à rentrer au bercail pour retrouver nos hôtes montpelliérains chéris :)   

Qui dit grosse randonnée, dit gros repas du soir : nous allons donc découvrir l'un des sanctuaire de Noémie et Pierre : le BIG MAMA et ses bonnes bières et gros burgers !! Nous nous sommes régalés, en effet c'est très bon !!  
Estomacs bien remplis, nous allons pouvoir rentrer dormir !

Bonne nuit !
