+++
title = "Pamiers, la tendre famille Verslip"
date = "2017-09-06"
tags = []
categories = ["Amis"]
banner = "img/blog/pamiers_head_web.jpg"
+++

Entre Hiis et Pamiers, nous avons traversé une partie de l'Ariège, très belle campagne verte et vallonnée. 

Nous sommes accueillis par ma chère Caro et sa tendre Suzanne : 10 mois, 4 dents, des joues à croquer !
Nous posons le camp chez elles du mercredi 6 au vendredi 8 septembre. 

Pendant ces jours, nous avons essayé de refaire le monde à notre manière, de grandes discussions passionnées et passionnantes. Malheureusement, nous n'avons pas encore trouvé la solution idéale pour soigner tous les maux, mais nous allons y travailler tous ensemble !

Petite promenade au Mas d'Asil pour prendre l'air, profiter de ce beau coin de France et surtout, aller voir LE dolmen ! Big up pour l'épicier du village, super sympa, proposant des produits ariégeois délicieux, parfait pour notre casse-croûte !

Nous avons aussi passé beaucoup de temps à observer la jolie Suzanne ! Titi était gaga, elle aussi je crois ! Après quelques minutes d'adaptation au bonhomme poilu, les voilà presque inséparables !! Suzanne est devenue une experte du rangement de DVD et de tout autre objet se présentant sur son passage ! 

<a href="/tdf/img/blog/titi_suzanne_web.jpg"> <img src="/tdf/img/blog/titi_suzanne_web.jpg" class="img-thumbnail center-block" alt="Titi&Suzanne" width="50%"></a>

Pour qualifier ces moments ariégeois nous pouvons choisir les mots suivants : douceur, rires, compotes maisons, découvertes, dolmen, bébé cascadeur, journée pyjama et spritz !

Merci pour ton accueil ma gitane d'amour <3

<a href="/tdf/img/blog/menhir_web.jpg"> <img src="/tdf/img/blog/menhir_web.jpg" class="img-thumbnail center-block" alt="Menhir" width="60%"></a>

<a href="/tdf/img/blog/menhir_2_web.jpg"> <img src="/tdf/img/blog/menhir_2_web.jpg" class="img-thumbnail center-block" alt="Menhir2" width="60%"></a>

<a href="/tdf/img/blog/orchidee_web.jpg"> <img src="/tdf/img/blog/orchidee_web.jpg" class="img-thumbnail center-block" alt="Orchidee" width="60%"></a>