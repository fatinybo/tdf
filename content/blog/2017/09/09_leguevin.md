+++
title = "Week-end retrouvailles en famille"
date = "2017-09-09"
tags = []
categories = ["Famille"]
banner = "img/blog/noemie_fanny_web.jpg"
+++

Noémie & Pierre récupérés, let's go to Leguevin (31) !

Trop cool, le premier soir tata Nicole nous régale, entourés de Christophe, Annick et Sébastien.
Ma cousine Annick se préparait psychologiquement à aller faire une journée vélo de rando le lendemain avec son chéri Sébastien. Lui, apparemment expert dans la discipline, elle un peu moins assurée ! 

Dimanche matin, soucieux de notre forme physique (!), footing dans la forêt de Boucone. Je pense que Nicole et Christophe auraient pu nous paumer à plusieurs reprises, mais tout s'est bien fini ! Bonne petite course en famille, qui nous permet de continuer notre tour de France des spots de footing !   
Et maintenant, au tour du cousin, Yann, et de sa chérie, "Moumou" d'arriver ! 
Encore une bien belle journée : bien parler, bien partager, bien manger, bien rire ! Week-end conclut à l'Isle-Jourdain (de mon coeur!), aux portes du Gers, pour un resto de CANARD !!! Nous avons bien mangé et bien bu, nous avions la peau du ventre bien tendue ! Petite pensée pour Nicole et son assiette de profiteroles, qui aurait pu nourrir notre table entière ! Retour au bercail à 4 dans la [104](https://www.google.fr/search?q=104&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiEycmk6afWAhVEnBoKHdLRBGUQ_AUICygC&biw=1920&bih=987#imgrc=GpLsqOrm4pH_TM:), sacrée sortie !   

Bonne adresse : [Clic](http://www.restaurantlesoliviers.net/accueil)

<a href="/tdf/img/blog/chaussures_web.jpg"> <img src="/tdf/img/blog/chaussures_web.jpg" class="img-thumbnail center-block" alt="Chaussures" width="40%"></a>

Lundi matin, l'objectif était d'aller au marché de Castelnaudary : capitale du cassoulet !!! Finalement, manque de temps, on se met en route sur les coups de midi en direction du sud-est ! Nous devons être à Montpellier à 14h mardi matin car Titi a rdv chez le dentiste !

Merci encore à Nicole pour son accueil royal :)

<a href="/tdf/img/blog/famille_web.jpg"> <img src="/tdf/img/blog/famille_web.jpg" class="img-thumbnail center-block" alt="Famille" width="80%"></a>

PS : pour tous ceux qui s'inquiétaient, nous avons eu des nouvelles de Annick : quelques caresses aux arbres mais rien de cassé : ni elle, ni le vélo !