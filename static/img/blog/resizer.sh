#!/bin/bash
#
# by inattendu
#
for image in *.jpg; do
	target=$(echo ${image}|grep -v web) 
	if [ ! -z ${target} ]; then
		convert ${target} -resize 1200x -quality 80 -set filename:f '%t_web.%e' +adjoin '%[filename:f]'
		rm ${target}
		echo "${target} --> Done"
	fi
done

